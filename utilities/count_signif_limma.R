count_signif <- function(res.limma, cutoff = 0.05, stat = "adj.P.Val", estimator = "logFC") {
  sig.over <- dim(res.limma[res.limma[, estimator] > 0 & res.limma[, stat] < cutoff, ])[1]

  sig.under <- dim(res.limma[res.limma[, estimator] < 0 & res.limma[, stat] < cutoff, ])[1]

  no.diff <- dim(res.limma)[1] - (sig.over + sig.under)

  v <- c(sig.under, no.diff, sig.over)
  names(v) <- c("Under", "Not DE", "Over")

  return(v)
}
