################################################################
##                      Create logFC matrix                    #
################################################################

rm(list = ls())
setwd(dirname(rstudioapi::getSourceEditorContext()$path))

# Comparisons
contrasts <- c(
  "Moderate_Acute_vs_Control", "Moderate_Subacute_vs_Control",
  "Moderate_Early_chronic_vs_Control", "Moderate_Late_chronic_vs_Control",
  "Severe_Acute_vs_Control", "Severe_Subacute_vs_Control",
  "Severe_Early_chronic_vs_Control", "Severe_Late_chronic_vs_Control"
)

result_list <- list()

allres <- data.frame()
allsignif <- data.frame()

maindir <- getwd()
metadir <- file.path(maindir, "meta-analysis_genes")

for (cont in 1:length(contrasts)) {
  metares <- read.csv(file.path(metadir, contrasts[cont], paste0(contrasts[cont], "_all.genes.tsv")),
    header = TRUE, sep = "\t", colClasses = c("Gene" = "character")
  )
  signifres <- metares[metares$p.adjust.fdr < 0.05, ]
  signifres$Group <- contrasts[cont]
  allsignif <- rbind(allsignif, signifres)

  result_list[[cont]] <- metares
  names(result_list)[[cont]] <- contrasts[cont]

  sub <- metares[, c("Gene", "logFC", "p.adjust.fdr")]
  sub$Contrast <- as.factor(contrasts[cont])
  allres <- rbind(allres, sub)
}

### generating matrix with all logFC for all studies
genes <- unique(allres$Gene)
mat.logFC <- matrix(NA, nrow = length(genes), ncol = length(result_list))
rownames(mat.logFC) <- genes
colnames(mat.logFC) <- names(result_list)

for (i in 1:length(result_list)) {
  co <- names(result_list[i])
  res <- result_list[[i]]

  logFC <- res$logFC

  names(logFC) <- res$Gene
  mat.logFC[, co] <- logFC[rownames(mat.logFC)]
}

# Selecting genes evaluated in all groups and significant in at least one group
signifgenes <- unique(allsignif$Gene)

df <- as.data.frame(mat.logFC[row.names(mat.logFC) %in% signifgenes, ])
df <- na.exclude(df)

write.table(df, "utilities/logFC_matrix.txt", row.names = T, col.names = T, sep = "\t")
